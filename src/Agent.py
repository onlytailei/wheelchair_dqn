#!/usr/bin/env python
import keras
from keras.models import model_from_config, Sequential, Model, model_from_config
from keras import backend as K


######################
# Agent
######################
# -------------------------------------------------------------------------
class Agent(object):

    def __init__(self, critic, actor, memory, num_actions, env, processor, random_process, target_update = 0.01):
        self.critic = critic
        self.actor = actor
        self.target_critic = self.create_target(critic)
        self.target_actor = self.create_target(actor)

        self.env = env
        self.memory = memory
        self.target_update = target_update
        self.step = 0
        self.processor = None
        self.random_process = None

        self.reset = 1



    def train(self, steps, ep_steps, warm_up):

        for self.step in range(0, steps):
            print("Step: " + str(self.step))

            if self.reset = 1:
                state0 = self.env.reset()

            if self.step < warm_up:
                actions = self.select_action(state0, steps)







    def select_action(self, state, steps):
        if self.processor == None:
            batch = state
        else:
            batch = self.process_state_batch(state)
        actions = self.actor.predict_on_batch(batch).flatten()
        if self.random_process is not None:
            noise = self.random_process.sample()
            noise_factor = 1 - self.step/steps
            actions = actions + noise * noise_factor
        return actions


    def create_target(self, model):
        config = {
            'class_name': model.__class__.__name__,
            'config': model.get_config(),
        }
        clone = model_from_config(config)
        clone.set_weights(model.get_weights())
        return clone

    def update_target_hard(self):
        self.target_critic.set_weights(self.critic.get_weights())
        self.target_actor.set_weights(self.actor.get_weights())

    def update_target_soft(self, model, target):
        weights = model.get_weights()
        target_weights = target.get_weights()
        for i in xrange(len(weights)):
            target_weights[i] = self.target_update*weights[i] + (1 - self.target_update)*target_weights[i]
        target.set_weights(target_weights)

    def load_weights(self):

    def save_weights(self):

    def feedforward(self):
