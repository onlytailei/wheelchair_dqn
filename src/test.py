#!/usr/bin/env python

# Needed for tf compatibility with keras
import tensorflow as tf
tf.python.control_flow_ops = tf

import keras
import numpy as np
from keras.layers import *
from keras.layers.advanced_activations import LeakyReLU as leaky
from keras.optimizers import RMSprop

from rl.agents import DDPGAgent
from rl.memory import SequentialMemory
from net import *
from environment import *

from Simulator import *

__DEBUG__ = True
dim_input = 84
frames = 4
num_actions = 4
ENV_NAME = 'WheelChair'

if __DEBUG__:
    print("Creating actor.")
actor = ActorNet(dim_input, num_actions, frames)
if __DEBUG__:
    print("Creating critic.")
critic = CriticNet(dim_input, num_actions, frames)

if __DEBUG__:
    print("Creating simulator")
sim = Simulator_interface()

if __DEBUG__:
    print("Creating env.")
env = Environment(frames, dim_input)

if __DEBUG__:
    print("Creating memory.")
memory = SequentialMemory(limit=100000, window_length=1)

if __DEBUG__:
    print("Creating agent.")
agent = DDPGAgent(nb_actions = num_actions, actor = actor.actor_net, critic = critic.critic_net, critic_action_input = critic.actions_input, memory = memory)

if __DEBUG__:
    print("Compiling agent.")
agent.compile([RMSprop(lr=.001), RMSprop(lr=.001)])

if __DEBUG__:
    print("Loading weights")
agent.load_weights('CHAIR_{}_weights.h5')

if __DEBUG__:
    print("Testing")
agent.test(env, nb_episodes=5, visualize=True, nb_max_episode_steps=1000)
