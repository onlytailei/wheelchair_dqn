#!/usr/bin/env python

# Needed for tf compatibility with keras
import tensorflow as tf
tf.python.control_flow_ops = tf

import os
import keras
import numpy as np
from keras.layers import *
from keras.layers.advanced_activations import LeakyReLU as leaky
from keras.optimizers import RMSprop, Adam, SGD

from rl.agents import DDPGAgent
from rl.memory import SequentialMemory
from rl.random import *
from rl.callbacks import FileLogger
from net import *
from environment import *
from Utils import MultiInputProcessor, memory_size
from Simulator import *

# Avoids TF to allocate all GPU.
# -------------------------------------------------------------------------
# import keras.backend.tensorflow_backend as KTF
#
# def get_session(gpu_fraction=0.9):
#
#     num_threads = os.environ.get('OMP_NUM_THREADS')
#     gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
#
#     if num_threads:
#         return tf.Session(config=tf.ConfigProto(
#             gpu_options=gpu_options, intra_op_parallelism_threads=num_threads))
#     else:
#         return tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))
#
# KTF.set_session(get_session())
# -------------------------------------------------------------------------


# Get userhome path for saving the images of the net structure
userhome = os.path.expanduser('~')

__DEBUG__ = False
dim_input = 34
replay = 45000

frames = 4
num_actions = 2
warm_up = 500
ENV_NAME = 'WheelChair'
reload_from_previous = False
weights_path = userhome + '/CHAIR_weights.h5'
processor = MultiInputProcessor(nb_inputs=4)
noise = OrnsteinUhlenbeckProcess(theta=.15, mu=0., sigma=0.3, size = num_actions) #  GaussianWhiteNoiseProcess(sigma=0.05, size = num_actions) #
logtofile = FileLogger('/home/paolo/dqn.log')

print("Checking memory replay size..")
memory_size(img_size = dim_input, replay_size = replay, max_ram = 6.5)

print("Creating actor.")
actor = ActorNet(dim_input, num_actions, frames)

print("Creating critic.")
critic = CriticNet(dim_input, num_actions, frames)

print("Creating env.")
env = Environment(frames, dim_input)

print("Creating memory.")
memory = SequentialMemory(limit = replay, window_length=1)

print("Creating agent.")
agent = DDPGAgent(nb_actions = num_actions, actor = actor.actor_net, critic = critic.critic_net, critic_action_input = critic.actions_input,
                memory = memory, nb_steps_warmup_critic=warm_up, nb_steps_warmup_actor=warm_up, gamma=.99, batch_size = 32, tester = actor.tester,
                target_model_update=.00001, processor=processor, random_process=noise, beta = None) # beta = 0.5 lets the normalized target to be in [-1, 1]

print("Compiling agent.")
agent.compile([RMSprop(lr=.0000001), RMSprop(lr=.0000001)]) #Adam is the optimizer

if reload_from_previous:
    print("Loading weights")
    agent.load_weights(weights_path)

print("Fit agent")
agent.fit(env, nb_steps=5000000, nb_max_episode_steps=150, callbacks = logtofile, verbose=2)

print("Save weights")
agent.save_weights(weights_path, overwrite=True)
#
# # Finally, evaluate our algorithm for 5 episodes.
# print("Testing")
# agent.test(env, nb_episodes=5, nb_max_episode_steps=1000)
