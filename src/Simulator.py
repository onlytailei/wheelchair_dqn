#!/usr/bin/env python
# -*- coding: utf-8 -*-
import rospy
import numpy as np
import random
from std_msgs.msg import Float32MultiArray, MultiArrayLayout, MultiArrayDimension, Float32, Bool, Int32
from dqn.srv import *
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Vector3Stamped
from collections import deque #list-like container with fast appends and pops on either end

import pylab as pl

import message_filters


__DEBUG__ = False

class Simulator_interface(object):
    ###############
    # Init class
    ###############
    # -------------------------------------------------------------------------
    def __init__(self, shape):
        # All radius are in meters
        self.rad_main_track = 0.11749/2
        self.rad_flipper_0 = 0.21736/2
        self.rad_flipper_1 = 0.18/2
        self.rad_flipper_2 = 0.145/2
        self.rad_flipper_3 = 0.11907/2

        self.desired_speed = 0.4 # In m/s (Fixed cause is not something we can change)
        self.speed_vect = []

        self.frames = 4
        self.distance = 0
        self.image_shape = [shape, shape]

        self.sim_time = 0 # Needed for reward
        self.sim_state = 0
        self.finished = False
        self.fail = False
        # Instantiate node
        rospy.init_node('simulator')

        # Model controls
        self.speed_pub = rospy.Publisher("motorSpeed", Float32MultiArray, queue_size = 10, latch = True)
        self.FL_flipper_pub = rospy.Publisher("FL_flipper", Float32, queue_size = 1, latch = True)
        self.BL_flipper_pub = rospy.Publisher("BL_flipper", Float32, queue_size = 1, latch = True)
        self.FR_flipper_pub = rospy.Publisher("FR_flipper", Float32, queue_size = 1, latch = True)
        self.BR_flipper_pub = rospy.Publisher("BR_flipper", Float32, queue_size = 1, latch = True)

        # Simulator controls
        self.start_simulation = rospy.Publisher("startSimulation", Bool, queue_size = 1)
        self.pause_simulation = rospy.Publisher("pauseSimulation", Bool, queue_size = 1)
        self.stop_simulation = rospy.Publisher("stopSimulation", Bool, queue_size = 1)
        self.simulation_state_sub = rospy.Subscriber("simulationState", Int32, self.handle_state)

        self.main_service = rospy.Service('ActionState_srv', ActionState, self.handle_request)
    # -------------------------------------------------------------------------


    ###################
    # Set motor speed
    ###################
    # -------------------------------------------------------------------------
    def set_speed(self):
        """Function that sets the speed of each wheel in the tracks.
        The mode flag is used to specify if we are using the flippers or not.
        If we don't use them we just need the back wheels.
        Mode == 1 if flippers
        Mode == 0 if no flippers"""
        motor_speed_vector = np.zeros((16)) # In deg/s

        # angular momentum = velocity/radius
        motor_speed_vector[0] = self.desired_speed/self.rad_flipper_3
        motor_speed_vector[1] = self.desired_speed/self.rad_flipper_2
        motor_speed_vector[2] = self.desired_speed/self.rad_flipper_1
        motor_speed_vector[3] = self.desired_speed/self.rad_flipper_0
        for i in range(4, 12):
            motor_speed_vector[i] = self.desired_speed/self.rad_main_track
        motor_speed_vector[12] = self.desired_speed/self.rad_flipper_0
        motor_speed_vector[13] = self.desired_speed/self.rad_flipper_1
        motor_speed_vector[14] = self.desired_speed/self.rad_flipper_2
        motor_speed_vector[15] = self.desired_speed/self.rad_flipper_3
        self.speed_vect = motor_speed_vector.tolist()
        # Prepare message to send
        message = Float32MultiArray()
        message.layout.dim.append(MultiArrayDimension())
        message.layout.dim[0].size = np.size(self.speed_vect)
        message.layout.dim[0].stride = 0
        del message.data[:]
        message.data = self.speed_vect
        self.speed_pub.publish(message)
    # -------------------------------------------------------------------------

    ################
    # Handle state
    ################
    # -------------------------------------------------------------------------
    def handle_state(self, msg):
        self.sim_state = msg.data
    # -------------------------------------------------------------------------

    ################
    # Handle time
    ################
    # -------------------------------------------------------------------------
    def handle_time(self, msg):
        self.sim_time = msg.data
    # -------------------------------------------------------------------------

    ################
    # Save Images
    ################
    # TODO cambia il salvataggio e la trasmissione dello shape dell immagine NB le immagini sono già piatte!!!
    # -------------------------------------------------------------------------
    def save_images(self, msg, inbuffer):
        """Catches the images at the right frequency and saves them in a buffer"""
        if np.shape(inbuffer)[0] >= self.frames:
            inbuffer.popleft()
        inbuffer.append(msg.channels[0].values)
    # -------------------------------------------------------------------------

    ################
    # Save Data
    ################
    # -------------------------------------------------------------------------
    def save_data(self, msg, inbuffer):
        """Catches the data at the right frequency and saves them in a buffer"""
        data = [msg.vector.x, msg.vector.y, msg.vector.z]
        if np.shape(inbuffer)[0] >= self.frames:
            inbuffer.popleft()
        inbuffer.append(data)
    # -------------------------------------------------------------------------

    #########################
    # Save messages callback
    #########################
    # -------------------------------------------------------------------------
    def save_messages(self, front_cam, back_cam, accel, gyro, (front_buff, back_buff, acc_buff, gyro_buff)):
        """This function is the callback for the syncronized cameras and sensors messages
        reception"""
        self.save_images(front_cam, front_buff)
        self.save_images(back_cam, back_buff)
        self.save_data(accel, acc_buff)
        self.save_data(gyro, gyro_buff)
    # -------------------------------------------------------------------------


    ##############
    # Get images
    ##############
    # -------------------------------------------------------------------------
    def get_images(self, inbuffer):
        """Return buffer reshaped in a 1-dim list."""
        images = np.array(inbuffer)
        return images.flatten().tolist()
    # -------------------------------------------------------------------------

    ###########################
    # Get sensor data variation
    ###########################
    # -------------------------------------------------------------------------
    def get_data(self, inbuffer):
        """Returns the average of the last 4 sensor lectures. This way we smooth the
        noise of the sensor, but we can penalise too high values."""
        x = 0
        y = 0
        z = 0
        for data in inbuffer:
            x += data[0]
            y += data[1]
            z += data[2]
        x = x/np.shape(inbuffer)[0]
        y = y/np.shape(inbuffer)[0]
        z = z/np.shape(inbuffer)[0]
        return [x, y, z]
    # -------------------------------------------------------------------------

    ##################
    # Handle distance
    ##################
    # -------------------------------------------------------------------------
    def handle_distance(self, msg):
        """Saves the distance readings in the class"""
        self.distance = msg.data
    # -------------------------------------------------------------------------

    #################
    # Handle end
    #################
    # -------------------------------------------------------------------------
    def handle_end(self, msg):
        """Saves the info on terminal state"""
        self.finished = msg.data
    # -------------------------------------------------------------------------

    #################
    # Handle fail
    #################
    # -------------------------------------------------------------------------
    def handle_fail(self, msg):
        """Saves the info on fail state"""
        self.fail = msg.data
    # -------------------------------------------------------------------------


    ##################
    # Handle request
    ##################
    # -------------------------------------------------------------------------
    def handle_request(self, srv):
        """Handle the request from the environment, once every step. Publishes the
        actions and receives the response from V-Rep."""

        # Second condition needed otherwise the stop can arrive after the start
        if srv.reset == True and not self.sim_state == 0:
            self.stop_simulation.publish(True)
            if __DEBUG__:
                print("Reset")
            # Wait for simulation to be stopped
            while not (self.sim_state == 0):
                continue

        self.start_simulation.publish(True)

        # Wait until simulation starts (Nedeed cause otherwise sometimes it remains paused)
        while not (self.sim_state == 1):
            continue

        self.set_speed()
        self.frames = srv.frames
        # Publish actions
        self.FL_flipper_pub.publish(srv.actions[0])
        self.BL_flipper_pub.publish(srv.actions[1])
        self.FR_flipper_pub.publish(srv.actions[0])
        self.BR_flipper_pub.publish(srv.actions[1])
        # After having published the actions we need some time to apply them
        # rospy.sleep(0.5)

        front_buffer = deque()
        back_buffer = deque()
        accel_buffer = deque()
        gyro_buffer = deque()

        front_sub = message_filters.Subscriber("front_depth", PointCloud)
        back_sub = message_filters.Subscriber("back_depth", PointCloud)
        accel_sub = message_filters.Subscriber("accelerometer", Vector3Stamped)
        gyro_sub = message_filters.Subscriber("gyro", Vector3Stamped)

        time_filter = message_filters.ApproximateTimeSynchronizer([front_sub, back_sub, accel_sub, gyro_sub], 1, 1)
        time_filter.registerCallback(self.save_messages, (front_buffer, back_buffer, accel_buffer, gyro_buffer))

        distance_sub = rospy.Subscriber("distance", Float32, self.handle_distance)
        terminal_sub = rospy.Subscriber("terminal", Bool, self.handle_end)
        fail_sub = rospy.Subscriber("fail", Bool, self.handle_fail)
        simulation_time_sub = rospy.Subscriber("simulationTime", Float32, self.handle_time)

        # Wait until we get the amount of data needed. CONSIDER IMPROVING/CHANGING THIS
        while not (np.shape(front_buffer)[0] == self.frames and np.shape(back_buffer)[0] == self.frames and
                    np.shape(accel_buffer)[0] == self.frames and np.shape(gyro_buffer)[0] == self.frames):
            rospy.sleep(0.1)

        self.pause_simulation.publish(True)
        # Wait until simulation is paused
        while not (self.sim_state == 2):
            continue

        # Needed cause rospy otherwise will leak memory
        front_sub.unregister()
        back_sub.unregister()
        accel_sub.unregister()
        gyro_sub.unregister()
        distance_sub.unregister()
        terminal_sub.unregister()
        fail_sub.unregister()
        simulation_time_sub.unregister()

        #Now fill the Response
        frames_front = self.get_images(front_buffer)
        frames_back = self.get_images(back_buffer)
        shape = self.image_shape
        accel = self.get_data(accel_buffer)
        gyro = self.get_data(gyro_buffer)
        terminal_state = self.finished
        failed = self.fail

        return [frames_front, frames_back, shape, self.distance, accel, gyro, terminal_state, failed, self.sim_time]
    # -------------------------------------------------------------------------


if __name__ == "__main__":
    sim = Simulator_interface(84)
    rospy.spin()
