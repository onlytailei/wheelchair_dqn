#!/usr/bin/env python
import numpy as np
from collections import deque #list-like container with fast appends and pops on either end

import rospy
from std_msgs.msg import Float32MultiArray, MultiArrayLayout, MultiArrayDimension, Float32, Bool, Int32
from sensor_msgs.msg import PointCloud
from geometry_msgs.msg import Vector3Stamped
import message_filters

from rl.core import Env

__DEBUG__ = False


class Environment(Env):
    """This class interfaces DQN with the simulation, publishing the actions
    and listening to sensor readings. It acts as the Env class, giving the opportunity
    to communicate with the ROS simulator node."""
    ###################
    # Init
    ###################
    # --------------------------------------------------------------------------
    def __init__(self, frames, input_shape):

        # REWARD
        # ----------------------------------------------------------------------
        # Numbers choosen randomly
        self.g_x = 5
        self.g_y = 4
        self.g_z = 3
        self.a_x = 2
        self.a_y = 3
        self.a_z = 0
        self.fp = 0.5

        self.initial_actions = [1, 1] # Initial flipper configuration
        self.terminal_reward = 1000
        self.old_distance = 0
        # ----------------------------------------------------------------------

        self.state = None
        self.frames = frames
        self.img_shape = input_shape

        self.reward_range = (-np.inf, np.inf)
        self.action_space = np.shape(self.initial_actions)
        self.observation_space = [self.frames, self.img_shape, self.img_shape]

        # Instantiate node
        rospy.init_node('simulator')
        self.new_data = False

        # SPEED
        # ----------------------------------------------------------------------
        # All radii are in meters
        self.rad_main_track = 0.11749/2
        self.rad_flipper_0 = 0.21736/2
        self.rad_flipper_1 = 0.18/2
        self.rad_flipper_2 = 0.145/2
        self.rad_flipper_3 = 0.11907/2

        self.desired_speed = 0.4 # In m/s (Fixed cause is not something we can change)
        self.speed_vect = []
        # ----------------------------------------------------------------------


        # MESSAGES DATA CONTAINERS
        # ----------------------------------------------------------------------
        self.distance = 0
        self.sim_state = 0
        self.finished = False
        self.fail = False
        self.sim_time = 0

        # Specifying the maxlen the buffer will pop-out from the left the oldest elements when it reaches the size
        self.front_buffer = deque(maxlen = self.frames)
        self.back_buffer = deque(maxlen = self.frames)
        self.accel_buffer = deque(maxlen = self.frames)
        self.gyro_buffer = deque(maxlen = self.frames)
        # ----------------------------------------------------------------------


        # PUBLISHER
        # ----------------------------------------------------------------------
        # Model controls
        self.speed_pub = rospy.Publisher("motorSpeed", Float32MultiArray, queue_size = 10, latch = True)
        self.FL_flipper_pub = rospy.Publisher("FL_flipper", Float32, queue_size = 1, latch = True)
        self.BL_flipper_pub = rospy.Publisher("BL_flipper", Float32, queue_size = 1, latch = True)
        self.FR_flipper_pub = rospy.Publisher("FR_flipper", Float32, queue_size = 1, latch = True)
        self.BR_flipper_pub = rospy.Publisher("BR_flipper", Float32, queue_size = 1, latch = True)
        # Simulator controls
        self.start_simulation = rospy.Publisher("startSimulation", Bool, queue_size = 1)
        self.pause_simulation = rospy.Publisher("pauseSimulation", Bool, queue_size = 1)
        self.stop_simulation = rospy.Publisher("stopSimulation", Bool, queue_size = 1)
        # ----------------------------------------------------------------------


        # SUBSCRIBER
        # ----------------------------------------------------------------------
        self.simulation_state_sub = rospy.Subscriber("simulationState", Int32, self.handle_state)
        self.distance_sub = rospy.Subscriber("distance", Float32, self.handle_distance)
        self.terminal_sub = rospy.Subscriber("terminal", Bool, self.handle_end)
        self.fail_sub = rospy.Subscriber("fail", Bool, self.handle_fail)
        self.simulation_time_sub = rospy.Subscriber("simulationTime", Float32, self.handle_time)

        # Syncronized Data Subscribers
        self.front_sub = message_filters.Subscriber("front_depth", PointCloud)
        self.back_sub = message_filters.Subscriber("back_depth", PointCloud)
        self.accel_sub = message_filters.Subscriber("accelerometer", Vector3Stamped)
        self.gyro_sub = message_filters.Subscriber("gyro", Vector3Stamped)
        self.time_filter = message_filters.ApproximateTimeSynchronizer([self.front_sub, self.back_sub, self.accel_sub, self.gyro_sub], 1, 1)
        self.time_filter.registerCallback(self.handle_messages, (self.front_buffer, self.back_buffer, self.accel_buffer, self.gyro_buffer))
        # ----------------------------------------------------------------------
    # -------------------------------------------------------------------------


    ####################
    # Publish Functions
    ####################
    # -------------------------------------------------------------------------
    def set_speed(self):
        """Function that sets the speed of each wheel in the tracks.
        The mode flag is used to specify if we are using the flippers or not.
        If we don't use them we just need the back wheels.
        Mode == 1 if flippers
        Mode == 0 if no flippers"""
        motor_speed_vector = np.zeros((16)) # In deg/s

        # angular momentum = velocity/radius
        motor_speed_vector[0] = self.desired_speed/self.rad_flipper_3
        motor_speed_vector[1] = self.desired_speed/self.rad_flipper_2
        motor_speed_vector[2] = self.desired_speed/self.rad_flipper_1
        motor_speed_vector[3] = self.desired_speed/self.rad_flipper_0
        for i in range(4, 12):
            motor_speed_vector[i] = self.desired_speed/self.rad_main_track
        motor_speed_vector[12] = self.desired_speed/self.rad_flipper_0
        motor_speed_vector[13] = self.desired_speed/self.rad_flipper_1
        motor_speed_vector[14] = self.desired_speed/self.rad_flipper_2
        motor_speed_vector[15] = self.desired_speed/self.rad_flipper_3
        self.speed_vect = motor_speed_vector.tolist()
        # Prepare message to send
        message = Float32MultiArray()
        message.layout.dim.append(MultiArrayDimension())
        message.layout.dim[0].size = np.size(self.speed_vect)
        message.layout.dim[0].stride = 0
        del message.data[:]
        message.data = self.speed_vect
        self.speed_pub.publish(message)

    def set_actions(self, actions):
        """This function publishes the actions for each flipper"""
        self.FL_flipper_pub.publish(actions[0])
        self.BL_flipper_pub.publish(actions[1])
        self.FR_flipper_pub.publish(actions[0])
        self.BR_flipper_pub.publish(actions[1])
    # -------------------------------------------------------------------------


    ################
    # Handles
    ################
    # -------------------------------------------------------------------------
    def handle_state(self, msg):
        self.sim_state = msg.data

    def handle_time(self, msg):
        self.sim_time = msg.data

    def handle_distance(self, msg):
        self.distance = msg.data

    def handle_end(self, msg):
        self.finished = msg.data

    def handle_fail(self, msg):
        self.fail = msg.data

    def handle_messages(self, front_cam, back_cam, accel, gyro, (front_buff, back_buff, acc_buff, gyro_buff)):
        """This function is the callback for the syncronized cameras and sensors messages
        reception. It calls the handles for each Subscriber in the message_filters"""
        self.save_images(front_cam, front_buff)
        self.save_images(back_cam, back_buff)
        self.save_data(accel, acc_buff)
        self.save_data(gyro, gyro_buff)
        self.new_data = True

    def save_images(self, msg, inbuffer):
        """Catches the images at the right frequency and saves them in a buffer
        after having reshaped them"""
        img = np.array(msg.channels[0].values).reshape((self.img_shape, self.img_shape))
        inbuffer.append(img)

    def save_data(self, msg, inbuffer):
        """Catches the data at the right frequency and saves them in a buffer"""
        data = [msg.vector.x, msg.vector.y, msg.vector.z]
        inbuffer.append(data)
    # -------------------------------------------------------------------------


    ##############
    # Get data
    ##############
    # -------------------------------------------------------------------------
    def get_images(self, inbuffer):
        """Return buffer as a numpy array of flattened images. For now the depth images
        are transmitted as a 1D vector"""
        return np.array(inbuffer)

    def get_imu_data(self, inbuffer):
        """Returns the average of the last num_frames sensor lectures. This way we smooth the
        noise of the sensor, but we can penalise too high values."""
        x = 0
        y = 0
        z = 0
        for data in inbuffer:
            x += data[0]
            y += data[1]
            z += data[2]
        x = x/np.shape(inbuffer)[0]
        y = y/np.shape(inbuffer)[0]
        z = z/np.shape(inbuffer)[0]
        return [x, y, z]
    # -------------------------------------------------------------------------


    ###################
    # Reward Function
    ###################
    # -------------------------------------------------------------------------
    def _reward_function(self, sensors, distance, finished, fail, actions):
        """Calculates the reward of the state"""
        gyro_penalty = self.g_x * np.square(sensors[3]) + self.g_y * np.square(sensors[4]) + self.g_z * np.square(sensors[5])
        acc_penalty = self.a_x * (sensors[0] > 3) + self.a_y * np.abs(sensors[1]) # The 5 is almost sin(30)*9.81. So not angles on x axis bigger than 30
        # Reward for having moved forward
        traveled = distance - self.old_distance

        reward = - gyro_penalty - acc_penalty
        if np.abs( traveled > 0.1): # This if is to check that we are not stuck
            reward = reward + 3
        else:
            reward = reward - 3

        reward = reward + self.terminal_reward * finished - 1000 * fail

        self.old_distance = distance
        if __DEBUG__:
            print("Reward: " + str(reward))
            print('')
        return reward
    # -------------------------------------------------------------------------


    ######################
    # Final Status
    ######################
    # -------------------------------------------------------------------------
    def close(self):
        print("Finished, unregistering subscribers...")
        self.simulation_state_sub.unregister()
        self.distance_sub.unregister()
        self.terminal_sub.unregister()
        self.fail_sub.unregister()
        self.simulation_time_sub.unregister()
        self.front_sub.unregister()
        self.back_sub.unregister()
        self.accel_sub.unregister()
        self.gyro_sub.unregister()
        print("Done.")
    # -------------------------------------------------------------------------


    ######################
    # Perform Step
    ######################
    # -------------------------------------------------------------------------
    def step(self, action):
        """
        Args:
            action (object): an action provided by the environment
        Returns:
            observation (float list): agent's observation of the current environment
            reward (float) : amount of reward returned after previous action
            done (boolean): whether the episode has ended, in which case further step() calls will return undefined results
            info (dict): contains auxiliary diagnostic information (helpful for debugging, and sometimes learning)
        """
        action = np.array(action).tolist() # So to get a list no matter what is the format of actions
        if __DEBUG__:
            self.count += 1
            print("Step: " + str(self.count))
            print("Actions: " + str(action))

        try:
            self.set_speed()
            self.set_actions(action)
        except rospy.ROSInterruptException:
            print("Program aborted.")

        # Needed fpr the warmup otherwise we got too fast
        while not self.new_data:
            continue

        front_frames = self.get_images(self.front_buffer)
        back_frames = self.get_images(self.back_buffer)
        accel_data = self.get_imu_data(self.accel_buffer)
        gyro_data = self.get_imu_data(self.gyro_buffer)

        sensors = accel_data + gyro_data

        if __DEBUG__:
            print("Sensors reading: " + str(sensors))

        self.state = np.array([front_frames, back_frames, np.array(sensors), np.array(action)])
        finish = (self.finished or self.fail)
        reward = self._reward_function(sensors, self.distance, self.finished, self.fail, action)
        info = {'Shape':self.img_shape, 'Debug mode':__DEBUG__}
        self.new_data = False

        return self.state, reward, finish, info
    # -------------------------------------------------------------------------


    ######################
    # Reset Sistem
    ######################
    # -------------------------------------------------------------------------
    def reset(self):
        """
        Resets the state of the environment and returns an initial observation.
        Returns:
            observation (object): the initial observation of the space. (Initial reward is assumed to be 0.)
        """
        if __DEBUG__:
            print("Reset Simulator")

        self.front_buffer.clear()
        self.back_buffer.clear()
        self.accel_buffer.clear()
        self.gyro_buffer.clear()

        # Reset simulator
        self.stop_simulation.publish(True)
        # Wait for simulation to be stopped
        while not (self.sim_state == 0):
            continue

        self.start_simulation.publish(True)
        # Wait until simulation starts
        while not (self.sim_state == 1):
            continue

        self.set_speed()
        self.set_actions(self.initial_actions)

        # We need this at the reset step to be sure that we have enough data to start working
        while not (len(self.front_buffer) == self.frames and  len(self.back_buffer) == self.frames):
            continue

        assert len(self.back_buffer) == self.frames
        assert len(self.front_buffer) == self.frames

        front_frames = self.get_images(self.front_buffer)
        back_frames = self.get_images(self.back_buffer)
        accel_data = self.get_imu_data(self.accel_buffer)
        gyro_data = self.get_imu_data(self.gyro_buffer)
        self.old_distance = self.distance

        sensors = accel_data + gyro_data
        self.state = [front_frames, back_frames, np.array(sensors), np.array(self.initial_actions)]
        return self.state
    # -------------------------------------------------------------------------


if __name__ == "__main__":
    env = Environment(4, 4)

    for _ in range(0, 20):
        actions = [-0.5, 1, -0.5,1]
        print("Doing step with actions: "+str(actions))
        env.step(actions)

    rospy.sleep(3)
    print("Calling reset")
    env.reset()
    env.step(actions)
