#!/usr/bin/env python
from __future__ import print_function
import rospy
import numpy as np
from std_msgs.msg import Float32MultiArray, MultiArrayLayout, MultiArrayDimension, Float32
from sensor_msgs.msg import Joy


class tele_op(object):

    def __init__(self):
        self.speed_delta = 1.5
        self.flipper_delta = 0.1
        self.FL_flipper = 0
        self.FR_flipper = 0
        self.BL_flipper = 0
        self.BR_flipper = 0
        self.des_right_speed = 0
        self.des_left_speed = 0

        self.rad_main_track = 0.11749/2
        self.rad_flipper_0 = 0.21736/2
        self.rad_flipper_1 = 0.18/2
        self.rad_flipper_2 = 0.145/2
        self.rad_flipper_3 = 0.11907/2

        rospy.init_node('tele_op')
        self.joystick_sub = rospy.Subscriber("joy", Joy, self.joystick_callback)
        self.right_pub = rospy.Publisher("right_speed", Float32MultiArray, queue_size = 1)
        self.left_pub = rospy.Publisher("left_speed", Float32MultiArray, queue_size = 1)
        self.FL_pub = rospy.Publisher("FL_flipper", Float32, queue_size = 1)
        self.BL_pub = rospy.Publisher("BL_flipper", Float32, queue_size = 1)
        self.FR_pub = rospy.Publisher("FR_flipper", Float32, queue_size = 1)
        self.BR_pub = rospy.Publisher("BR_flipper", Float32, queue_size = 1)

    def pub_speed(self):
        right_speed = np.zeros((16))
        left_speed = np.zeros((16))
        # RIGHT
        right_speed[0] = self.des_right_speed/self.rad_flipper_3
        right_speed[1] = self.des_right_speed/self.rad_flipper_2
        right_speed[2] = self.des_right_speed/self.rad_flipper_1
        right_speed[3] = self.des_right_speed/self.rad_flipper_0
        for i in range(4, 12):
            right_speed[i] = self.des_right_speed/self.rad_main_track
        right_speed[12] = self.des_right_speed/self.rad_flipper_0
        right_speed[13] = self.des_right_speed/self.rad_flipper_1
        right_speed[14] = self.des_right_speed/self.rad_flipper_2
        right_speed[15] = self.des_right_speed/self.rad_flipper_3
        # LEFT
        left_speed[0] = self.des_left_speed/self.rad_flipper_3
        left_speed[1] = self.des_left_speed/self.rad_flipper_2
        left_speed[2] = self.des_left_speed/self.rad_flipper_1
        left_speed[3] = self.des_left_speed/self.rad_flipper_0
        for i in range(4, 12):
            left_speed[i] = self.des_left_speed/self.rad_main_track
        left_speed[12] = self.des_left_speed/self.rad_flipper_0
        left_speed[13] = self.des_left_speed/self.rad_flipper_1
        left_speed[14] = self.des_left_speed/self.rad_flipper_2
        left_speed[15] = self.des_left_speed/self.rad_flipper_3

        right_speed = right_speed.tolist()
        left_speed = left_speed.tolist()

        message_right = Float32MultiArray()
        message_right.layout.dim.append(MultiArrayDimension())
        message_right.layout.dim[0].size = np.size(right_speed)
        message_right.layout.dim[0].stride = 0
        del message_right.data[:]
        message_right.data = right_speed
        message_left = Float32MultiArray()
        message_left.layout.dim.append(MultiArrayDimension())
        message_left.layout.dim[0].size = np.size(left_speed)
        message_left.layout.dim[0].stride = 0
        del message_left.data[:]
        message_left.data = left_speed

        self.right_pub.publish(message_right)
        self.left_pub.publish(message_left)


    def front_flip_up(self):
        if(self.FL_flipper < 1.6):
            self.FL_flipper += self.flipper_delta
            self.FR_flipper += self.flipper_delta

    def front_flip_down(self):
        if(self.FL_flipper > -1.6):
            self.FL_flipper -= self.flipper_delta
            self.FR_flipper -= self.flipper_delta

    def back_flip_up(self):
        if(self.BL_flipper < 1.6):
            self.BL_flipper += self.flipper_delta
            self.BR_flipper += self.flipper_delta

    def back_flip_down(self):
        if(self.BL_flipper > -1.6):
            self.BR_flipper -= self.flipper_delta
            self.BL_flipper -= self.flipper_delta

    def pub_flippers(self):
        self.FL_pub.publish(self.FL_flipper)
        self.FR_pub.publish(self.FR_flipper)
        self.BL_pub.publish(self.BL_flipper)
        self.BR_pub.publish(self.BR_flipper)

    def joystick_callback(self, msg):
        speed = msg.axes[2:4]
        flippers = msg.buttons[4:8]
        # Set flippers positions
        if(flippers[0] == 1):
            self.back_flip_up()
        elif(flippers[2] == 1):
            self.back_flip_down()
        if(flippers[1] == 1):
            self.front_flip_up()
        elif(flippers[3] == 1):
            self.front_flip_down()

        # Set speed
        coeff = 1
        self.des_left_speed = coeff*(speed[1] - speed[0])
        self.des_right_speed = coeff*(speed[1] + speed[0])
        self.pub_speed()
        self.pub_flippers()


if __name__ == "__main__":
    teleoperator = tele_op()
    print("Ready to accept commands")
    rospy.spin()
