#!/usr/bin/env python

from collections import deque #list-like container with fast appends and pops on either end
import random
import numpy as np
import skimage
from skimage.transform import resize

######################
# Replay Buffer
######################
#--------------------------------------------------------------------------------------
class ReplayBuffer(object):
    """This class implements all the functions needed for the memory replay"""

    def __init__(self, buffer_size):
        """Initialize the buffer"""
        self.buffer_size = buffer_size
        self.count = 0
        self.buffer = deque()

    def add(self, s, a, r, t, s2):
        """Add experience to the buffer"""
        experience = (s, a, r, t, s2)
        if self.count < self.buffer_size: #if buffer not full yet
            self.buffer.append(experience)
            self.count += 1
        else:
            self.buffer.popleft()
            self.buffer.append(experience)

    def size(self):
        """Returns the size of the buffer"""
        return self.count

    def sample_batch(self, batch_size):
        """Samples the batch from the buffer and returns it"""
        bathc = []
        if self.count < batch_size:
            batch = random.sample(self.buffer, self.count)
        else:
            batch = random.sample(self.buffer, batch_size)

        s_batch = np.array([i[0] for i in batch])
        a_batch = np.array([i[1] for i in batch])
        r_batch = np.array([i[2] for i in batch])
        t_batch = np.array([i[3] for i in batch])
        s2_batch = np.array([i[4] for i in batch])
        return s_batch, a_batch, r_batch, t_batch, s2_batch

    def clear(self):
        """Reset the buffer and the count"""
        self.deque.clear()
        self.count = 0
#--------------------------------------------------------------------------------------


######################
# Check NAN
######################
# -------------------------------------------------------------------------
def check_nan(data, position):
    """Check if the data has nan, raising an exception listing the data
    and the place where it happend"""
    string = 'NAN in ' + str(position)
    if np.isnan(data).any():
        #print(data)
        raise NameError(string)
# -------------------------------------------------------------------------


######################
# Image Preprocessing
######################
# -------------------------------------------------------------------------
def preprocess_images(image, rescaled_img_size):
    """This function elaborates the RGB image so that it becomes a grayscale one
    of the desired size.
    It does not flatten the images."""
    check_nan(image, 'depth')
    # NB no resizing cause I give images of the right dim already from vrep. Cause resize was adding artifacts
    # image = np.array(resize(image, (rescaled_img_size, rescaled_img_size)))
    # check_nan(image, 'resize')
    return image
# -------------------------------------------------------------------------


######################
# State Processing
######################
# -------------------------------------------------------------------------
class Processor(object):
    def process_step(self, observation, reward, done, info):
        observation = self.process_observation(observation)
        reward = self.process_reward(reward)
        info = self.process_info(info)
        return observation, reward, done, info

    def process_observation(self, observation):
        """Processed observation will be stored in memory
        """
        return observation

    def process_reward(self, reward):
        return reward

    def process_info(self, info):
        return info

    def process_state_batch(self, batch):
        """Process for input into NN
        """
        return batch

    def process_action(self, action):
        return action

    @property
    def metrics_names(self):
        return []

    @property
    def metrics(self):
        return []


class MultiInputProcessor(Processor):
    def __init__(self, nb_inputs):
        self.nb_inputs = nb_inputs

    def process_state_batch(self, state_batch):
        input_batches = [[] for x in range(self.nb_inputs)]
        for state in state_batch:
            processed_state = None
            for observation in state:
                assert len(observation) == self.nb_inputs
                processed_state = observation
            for idx, s in enumerate(processed_state):
                input_batches[idx].append(s)
        return [np.array(x) for x in input_batches]
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
def memory_size(img_size, replay_size, max_ram):
    """Checks how much memory the memory replay requires.
    Change if change mem_size calculation if change replay memory."""
    img = np.ones((img_size, img_size, 4))
    sens = np.ones((6,1))
    a = np.ones((2,1))

    mem_size = (4*img.nbytes + 2*sens.nbytes + a.nbytes + 8)/1024.0

    print("Size of conv image: " + str(img_size-13))
    print("Memory in Kb of exp: " + str(mem_size))
    mem_Mb = mem_size/1024
    mem_Gb = replay_size*mem_Mb/1024
    print("Memory in Gb of replay: " + str(mem_Gb))

    if mem_Gb > max_ram:
        print("Too much RAM required for replay. Stopping")
        raise ValueError
    else:
        print("OK!")
# -------------------------------------------------------------------------
