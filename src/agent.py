#!/usr/bin/env python
import keras
import numpy as np

class Agent(object):
    """This is the agent that trains the networks and does the steps in the simulator"""

    def __init__(self, actor, critic, critic_action_inputs, memory_buffer, gamma=0.99, learning_rate=0.01,
                warm_up = 1000, target_update=0.001, batch_size=32):

        if batch_size > warm_up:
            raise ValueError('Warm up steps less than batch size. Consider increasing warm up. Batch size: {}'.format(batch_size))

        self.actor = actor
        self.critic = critic
        self.critic_actions = critic_action_inputs
        self.memory_buffer = memory_buffer
        self.gamma = gamma
        self.learning_rate = learning_rate
        self.warm_up = warm_up
        self.target_update = target_update
        self.batch_size = batch_size

        self.count_steps = 0
        self.count_ep_steps = 0




    def train(env, step_numbers, steps_per_episode = 1000):
        past_state = env.reset()

        while self.count_steps < step_numbers:
            if self.count_ep_steps < steps_per_episode and not finish:
                action = self.actor.predict(past_state)
                present_state, reward, finish, info = env.step(action)
                t = 0
                self.memory_buffer.add(past_state, action, reward, t, present_state)

                if self.count_steps > self.warm_up:
                    train_nets()

                past_state = present_state
                self.count_steps += 1
                self.count_ep_steps += 1
            else:
                past_state = env.reset()
                self.count_steps += 1
                self.count_ep_steps = 0
            # While for warm up is finished
