#!/usr/bin/env python
import keras
import skimage
import numpy as np
import os
from keras import backend as K
from keras.layers import *
from keras.models import Model
from keras.layers.advanced_activations import LeakyReLU as leaky
from keras.layers.normalization import BatchNormalization
from keras.regularizers import l2, activity_l1l2

__DEBUG__ = True
reg = 0.01

if __DEBUG__:
    from keras.utils.visualize_util import plot
    if keras.backend._BACKEND == 'theano':
        from theano.compile.nanguardmode import NanGuardMode
        mode = NanGuardMode(nan_is_error = True, inf_is_error = True, big_is_error = True)

    # Get userhome path for saving the images of the net structure
    userhome = os.path.expanduser('~')

# NB: For 3D conv nets the input shape is:
# 5D tensor with shape: (samples, channels (rgb=3 or bw=1), frames, width, height) if dim_ordering='th'
# 5D tensor with shape: (samples, frames, width, height, channels (rgb=3 or bw=1)) if dim_ordering='tf'.

# NB: Max pooling is better than Average cause evidentiates the edges (very useful for stairs)

######################
# Actor Net
######################
#--------------------------------------------------------------------------------------
class ActorNet(object):
    """This class is entitled in creating the neural network for the actions
    selection, that is the actor network."""

    def __init__(self, dim_input, dim_action = 4, frames = 4, soft_target = 0):
        #Parameters
        self.dim_input = dim_input
        self.dim_action = dim_action
        self.frames = frames
        self.soft_target = soft_target
        #Nets
        self.tester = None
        self.actor_net = self.create_actor_net()


    def create_actor_net(self):

        # Convolution branch for the front camera: 4 frames of dim_input x dim_input depth images
        # ----------------------------------------------------------------------
        image_input_front = Input(shape = (self.frames, self.dim_input, self.dim_input), name = 'Image_input_front_actor')

        conv_branch_front = Convolution2D(8, 8, 8, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(image_input_front)
        conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        # conv_branch_front = Convolution2D(16, 8, 8, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        # conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        conv_branch_front = Convolution2D(16, 4, 4, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        conv_branch_front = Convolution2D(32, 4, 4, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        # conv_branch_front = Convolution2D(32, 3, 3, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        # conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        conv_branch_front_f = Flatten()(conv_branch_front)
        # ----------------------------------------------------------------------


        # Convolution branch for the back camera: 4 frames of dim_input x dim_input depth images
        # ----------------------------------------------------------------------
        image_input_back = Input(shape = (self.frames, self.dim_input, self.dim_input), name = 'Image_input_back_actor')

        conv_branch_back = Convolution2D(8, 8, 8, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(image_input_back)
        conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        # conv_branch_back = Convolution2D(16, 8, 8, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        # conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        conv_branch_back = Convolution2D(16, 4, 4, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        conv_branch_back = Convolution2D(32, 4, 4, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        # conv_branch_back = Convolution2D(32, 3, 3, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        # conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        conv_branch_back_f = Flatten()(conv_branch_back)
        # ----------------------------------------------------------------------

        # IMU input
        # ----------------------------------------------------------------------
        sensors_input = Input(shape = (6,), name = 'Sensors_actor_input')
        sensors = BatchNormalization(mode = 2)(sensors_input)
        sensors = Dense(256, init = 'he_normal')(sensors)
        sensors = leaky(alpha = 0.01)(sensors)
        # ----------------------------------------------------------------------

        # act input
        # ----------------------------------------------------------------------
        prev_actions_input = Input(shape = (2,), name = 'Prev_actions_actor_in')
        act = BatchNormalization(mode = 2)(prev_actions_input)
        act = Dense(128, init = 'he_normal')(act)
        act = leaky(alpha = 0.01)(act)
        # ----------------------------------------------------------------------


        # ----------------------------------------------------------------------
        actor = merge([conv_branch_front_f, conv_branch_back_f, sensors, act], mode = 'concat', concat_axis = 1)
        actor = Dense(2048, init = 'he_normal')(actor)
        actor = leaky(alpha = 0.01)(actor)
        actor = Dropout(0.5)(actor)

        actor = Dense(512, init = 'he_normal')(actor)
        actor = leaky(alpha = 0.01)(actor)
        actor = Dropout(0.5)(actor)

        # actions = 4 tracked arms inclinations
        actor = Dense(self.dim_action, init = 'uniform', activation = 'linear')(actor)
        actor_output = Activation('tanh')(actor)

        actor_model = Model(input = [image_input_front, image_input_back, sensors_input, prev_actions_input], output = actor_output)
        # ----------------------------------------------------------------------

        self.tester = [Model(input = [image_input_front, image_input_back, sensors_input, prev_actions_input], output = actor),
                        Model(input = [image_input_front, image_input_back, sensors_input, prev_actions_input], output = conv_branch_front),
                        Model(input = [image_input_front, image_input_back, sensors_input, prev_actions_input], output = conv_branch_back)]

        if __DEBUG__:
            shape_filepath = userhome + '/actor.png'
            plot(actor_model, to_file = shape_filepath, show_shapes=True, show_layer_names=True)

        return actor_model
#--------------------------------------------------------------------------------------


######################
# Critic Net
######################
#--------------------------------------------------------------------------------------
class CriticNet(object):
    """This class is the one entitled in creating the deep neural net for
    for the Q-function, that is the critic, approximation"""

    def __init__(self, dim_input, dim_action = 4, frames = 4, soft_target = 0):
        #Parameters
        self.dim_input = dim_input
        self.dim_action = dim_action
        self.frames = frames
        self.soft_target = soft_target
        #Nets
        self.actions_input = 0
        self.critic_net = self.create_critic_net()

    def create_critic_net(self):
        # Convolution branch for the front camera: 4 frames of dim_input x dim_input depth images
        # ----------------------------------------------------------------------
        image_input_front = Input(shape = (self.frames, self.dim_input, self.dim_input), name = 'Image_input_front')

        conv_branch_front = Convolution2D(8, 8, 8, init = 'he_normal', W_regularizer=l2(reg), border_mode = 'valid', dim_ordering='th')(image_input_front)
        conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        # conv_branch_front = Convolution2D(16, 8, 8, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        # conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        conv_branch_front = Convolution2D(16, 4, 4, init = 'he_normal', W_regularizer=l2(reg), border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        conv_branch_front = Convolution2D(32, 4, 4, init = 'he_normal', W_regularizer=l2(reg), border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        # conv_branch_front = Convolution2D(32, 3, 3, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_front)
        # conv_branch_front = leaky(alpha = 0.01)(conv_branch_front)

        conv_branch_front = Flatten()(conv_branch_front)
        # ----------------------------------------------------------------------


        # Input of the convolution branch for the back camera: 4 frames of dim_input x dim_input depth images
        # ----------------------------------------------------------------------
        image_input_back = Input(shape = (self.frames, self.dim_input, self.dim_input), name = 'Image_input_back')

        conv_branch_back = Convolution2D(8, 8, 8, init = 'he_normal', W_regularizer=l2(reg), border_mode = 'valid', dim_ordering='th')(image_input_back)
        conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        # conv_branch_back = Convolution2D(16, 8, 8, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        # conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        conv_branch_back = Convolution2D(16, 4, 4, init = 'he_normal', W_regularizer=l2(reg), border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        conv_branch_back = Convolution2D(32, 4, 4, init = 'he_normal', W_regularizer=l2(reg), border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        # conv_branch_back = Convolution2D(32, 3, 3, init = 'he_normal', border_mode = 'valid', dim_ordering='th')(conv_branch_back)
        # conv_branch_back = leaky(alpha = 0.01)(conv_branch_back)

        conv_branch_back = Flatten()(conv_branch_back)
        # ----------------------------------------------------------------------

        # IMU input
        # ----------------------------------------------------------------------
        sensors_input = Input(shape = (6,), name = 'Sensors_critic_input')
        sensors = BatchNormalization(mode = 2)(sensors_input)
        sensors = Dense(256, init = 'he_normal', W_regularizer=l2(reg))(sensors)
        sensors = leaky(alpha = 0.01)(sensors)
        # ----------------------------------------------------------------------

        # act input
        # ----------------------------------------------------------------------
        prev_actions_input = Input(shape = (2,), name = 'Prev_actions_actor_in')
        act = BatchNormalization(mode = 2)(prev_actions_input)
        act = Dense(128, init = 'he_normal')(act)
        act = leaky(alpha = 0.01)(act)
        # ----------------------------------------------------------------------


        # ----------------------------------------------------------------------
        #Action inputs
        self.actions_input = Input(shape = (self.dim_action,), name = 'Action_input')
        #Merge actions and output of the conv parts
        critic = merge([conv_branch_front, conv_branch_back, sensors, act, self.actions_input], mode = 'concat', concat_axis = 1)
        critic = Dense(2048, init = 'he_normal', W_regularizer=l2(reg))(critic)
        critic = leaky(alpha = 0.01)(critic)
        critic = Dropout(0.5)(critic)

        critic = Dense(512, init = 'he_normal', W_regularizer=l2(reg))(critic)
        critic = leaky(alpha = 0.01)(critic)
        critic = Dropout(0.5)(critic)

        # The critic output is 1 cause we need just one Q value for vector of actions
        critic_output = Dense(1, init = 'uniform', activation='linear', W_regularizer=l2(reg))(critic)

        # This is the layer needed for the normalization of the output, according to https://arxiv.org/pdf/1602.07714
        critic_output = Dense(1, activation='linear', weights=[np.ones((1,1)), np.zeros((1,))], bias=True)(critic_output)

        critic_model = Model(input = [image_input_front, image_input_back, sensors_input, prev_actions_input, self.actions_input], output = critic_output)
        # ----------------------------------------------------------------------

        if __DEBUG__:
            shape_filepath = userhome + '/critic.png'
            plot(critic_model, to_file = shape_filepath, show_shapes=True, show_layer_names=True)

        return critic_model
#--------------------------------------------------------------------------------------
