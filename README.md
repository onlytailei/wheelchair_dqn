# README #

This is the code for the Reinforcement Learning project I worked on during my Research Assistant role at RAM Lab in Hong Kong City University.

### How do to set up and run? ###

To make the code run you need:

* ROS
* Python 2.7
* TensorFlow
* Keras
* V-Rep

Steps for running it:

* Download the code in the **src** folder of you *catkin workspace* and compile it with **catkin_make**.
* Launch **roscore**
* Run V-Rep with the Robot.ttt scene
* Launch the program with *rosrun dqn main.py*

**NB**

Roscore can be closed without having to close V-Rep, but when launching it again, you need to open and close the RosInterfaceHelper script in the scene, to make the connection with ROS.


### Who do I talk to? ###

**Repo owner/Developer:**

* Giuseppe Paolo

* Contact: giuseppe.paolo93@gmail.com